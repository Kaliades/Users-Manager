#User Manager
        It was an interview task for Faust It.
    
This is a simple web service that allows you to create a users, edit or delete them.   
It also lets you create groups and assign them to users. 

##Technologies:
* Programming langugage: Java
* Used frameworks:
    * Spring-boot 2.0
    * JPA-Hibernate
    * Thymeleaf
    * Bootstrap 4.0 
    
    
   