package com.simonkulinski.interviewtask.bootstrap;

import com.simonkulinski.interviewtask.model.GroupUsers;
import com.simonkulinski.interviewtask.model.User;
import com.simonkulinski.interviewtask.repository.GroupUsersRepository;
import com.simonkulinski.interviewtask.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private GroupUsersRepository groupUsersRepository;
    @Autowired
    private UserRepository userRepository;

    public DevBootstrap(GroupUsersRepository groupUsersRepository, UserRepository userRepository) {
        this.groupUsersRepository = groupUsersRepository;
        this.userRepository = userRepository;
    }


    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        initData();
    }

    private void initData() {
        //users

        User simon = new User("Simon","Simon","Kulinski","30-01-1995","password1");
        User bartek = new User("Bebe","Bartek","Koltun","18-04-1990","password2");
        User kamil = new User("Kamilek","Kamil","Krzysztof","12-04-1995","password3");
        User alex = new User("Aleksiej","Alex","Kowalski","30-01-1980","password4");


        //groups
        GroupUsers admin = new GroupUsers("Admin");
        GroupUsers normalUser = new GroupUsers("User");
        GroupUsers developer = new GroupUsers("Developer");

        //users to group
        simon.getGroups().add(admin);
        bartek.getGroups().add(developer);
        bartek.getGroups().add(normalUser);
        kamil.getGroups().add(normalUser);
        kamil.getGroups().add(admin);
        simon.getGroups().add(normalUser);
        alex.getGroups().add(admin);

        //users to group
        admin.getUsers().add(simon);
        admin.getUsers().add(kamil);
        admin.getUsers().add(alex);
        normalUser.getUsers().add(bartek);
        normalUser.getUsers().add(simon);
        normalUser.getUsers().add(kamil);
        developer.getUsers().add(bartek);

        //save groups repository
        groupUsersRepository.save(admin);
        groupUsersRepository.save(normalUser);
        groupUsersRepository.save(developer);

        //save user repository
        userRepository.save(bartek);
        userRepository.save(kamil);
        userRepository.save(alex);
        userRepository.save(simon);

    }

}