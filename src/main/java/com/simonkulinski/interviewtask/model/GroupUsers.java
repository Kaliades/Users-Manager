package com.simonkulinski.interviewtask.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class GroupUsers {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @ManyToMany(mappedBy = "groups")
    private List<User> users;

    public GroupUsers() {
        users = new ArrayList<>();
    }

    public GroupUsers(String name) {
        this.name = name;
        users = new ArrayList<>();
    }

    public GroupUsers(String name, List<User> users) {
        this.name = name;
        this.users = new ArrayList<>();
        this.users = users;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupUsers that = (GroupUsers) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "GroupUsers{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", users=" + users +
                '}';
    }
    @PreRemove
    private void removeGroupsFromUsers() {
        for (User u : users) {
            u.getGroups().remove(this);
        }
    }
}
