package com.simonkulinski.interviewtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleUserMenagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleUserMenagerApplication.class, args);
	}
}
