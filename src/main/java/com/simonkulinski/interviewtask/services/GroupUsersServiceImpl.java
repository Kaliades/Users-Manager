package com.simonkulinski.interviewtask.services;

import com.simonkulinski.interviewtask.commands.GroupUsersCommand;
import com.simonkulinski.interviewtask.conventers.GroupUsersCommandToGroupUsers;
import com.simonkulinski.interviewtask.conventers.GroupUsersToGroupUsersCommand;
import com.simonkulinski.interviewtask.model.GroupUsers;
import com.simonkulinski.interviewtask.repository.GroupUsersRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GroupUsersServiceImpl implements GroupUsersService {

    private GroupUsersRepository repository;
    private GroupUsersCommandToGroupUsers groupUsersCommandToGroupUsers;
    private GroupUsersToGroupUsersCommand groupUsersToGroupUsersCommand;

    public GroupUsersServiceImpl(GroupUsersRepository repository, GroupUsersCommandToGroupUsers groupUsersCommandToGroupUsers, GroupUsersToGroupUsersCommand groupUsersToGroupUsersCommand) {
        this.repository = repository;
        this.groupUsersCommandToGroupUsers = groupUsersCommandToGroupUsers;
        this.groupUsersToGroupUsersCommand = groupUsersToGroupUsersCommand;
    }


    @Override
    public List<GroupUsers> getAllGroup() {
        List<GroupUsers> list = new ArrayList<>();
        repository.findAll().forEach(list::add);
        return list;
    }

    @Override
    public GroupUsers getGroupById(Long id) {
        Optional<GroupUsers> group = repository.findById(id);
        if (!group.isPresent())
            throw new RuntimeException("Group users not found");
        return group.get();
    }

    @Override
    public GroupUsersCommand saveGroupUsers(GroupUsersCommand command) {
        GroupUsers toSave = groupUsersCommandToGroupUsers.convert(command);
        GroupUsers saved = repository.save(toSave);
        return groupUsersToGroupUsersCommand.convert(saved);
    }

    @Override
    public GroupUsersCommand findGroupUsersCommandById(Long id) {
        return groupUsersToGroupUsersCommand.convert(getGroupById(id));
    }

    @Override
    public void deleteGroupUsersById(Long id) {
        repository.deleteById(id);
    }
}
