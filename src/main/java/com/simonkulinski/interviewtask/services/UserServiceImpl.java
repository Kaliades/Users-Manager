package com.simonkulinski.interviewtask.services;

import com.simonkulinski.interviewtask.commands.UserCommand;
import com.simonkulinski.interviewtask.conventers.UserCommandToUser;
import com.simonkulinski.interviewtask.conventers.UserToUserCommand;
import com.simonkulinski.interviewtask.model.GroupUsers;
import com.simonkulinski.interviewtask.model.User;
import com.simonkulinski.interviewtask.repository.GroupUsersRepository;
import com.simonkulinski.interviewtask.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private GroupUsersRepository groupUsersRepository;
    private UserToUserCommand userToUserCommand;
    private UserCommandToUser userCommandToUser;

    public UserServiceImpl(UserRepository userRepository, GroupUsersRepository groupUsersRepository, UserToUserCommand userToUserCommand, UserCommandToUser userCommandToUser) {
        this.userRepository = userRepository;
        this.groupUsersRepository = groupUsersRepository;
        this.userToUserCommand = userToUserCommand;
        this.userCommandToUser = userCommandToUser;
    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        return users;
    }

    @Override
    public User getUserById(Long id) {
        Optional<User> user = userRepository.findById(id);
        if (!user.isPresent())
            throw new RuntimeException("User not fund");
        return user.get();
    }

    @Override
    public UserCommand saveUser(UserCommand command) {
        User saveUser = userCommandToUser.convert(command);
        User user = userRepository.save(saveUser);
        if (command.getGroupsId() != null && command.getGroupsId().size() > 0)
            for (Long aLong : command.getGroupsId()) {
                saveGroup(aLong, user);
            }

        user = userRepository.save(user);
        return userToUserCommand.convert(user);
    }

    @Override
    public UserCommand findUserCommandById(Long id) {
        return userToUserCommand.convert(getUserById(id));
    }

    @Override
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }

    private void saveGroup(Long id, User user) {
        GroupUsers groupUsers = groupUsersRepository.findById(id).get();
        groupUsers.getUsers().add(user);
        user.getGroups().add(groupUsers);
        groupUsersRepository.save(groupUsers);
    }
}
