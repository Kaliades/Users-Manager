package com.simonkulinski.interviewtask.services;

import com.simonkulinski.interviewtask.commands.GroupUsersCommand;
import com.simonkulinski.interviewtask.model.GroupUsers;

import java.util.List;

public interface GroupUsersService {

    List<GroupUsers> getAllGroup();

    GroupUsers getGroupById(Long id);

    GroupUsersCommand saveGroupUsers(GroupUsersCommand command);

    GroupUsersCommand findGroupUsersCommandById(Long id);

    void deleteGroupUsersById(Long id);

}
