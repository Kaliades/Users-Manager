package com.simonkulinski.interviewtask.services;

import com.simonkulinski.interviewtask.commands.UserCommand;
import com.simonkulinski.interviewtask.model.User;

import java.util.List;

public interface UserService {

    List<User> getAllUsers();
    User getUserById(Long id);
    UserCommand saveUser(UserCommand command);
    UserCommand findUserCommandById(Long id);
    void deleteUserById(Long id);


}
