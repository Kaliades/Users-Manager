package com.simonkulinski.interviewtask.conventers;

import com.simonkulinski.interviewtask.commands.GroupUsersCommand;
import com.simonkulinski.interviewtask.model.GroupUsers;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class GroupUsersToGroupUsersCommand implements Converter<GroupUsers, GroupUsersCommand> {

 /*   private final UserToUserCommand converter;

    public GroupUsersToGroupUsersCommand(UserToUserCommand converter) {
        this.converter = converter;
    }*/

    @Synchronized
    @Nullable
    @Override
    public GroupUsersCommand convert(GroupUsers groupUsers) {
        final GroupUsersCommand command = new GroupUsersCommand();
        command.setId(groupUsers.getId());
        command.setName(groupUsers.getName());

       /* if (groupUsers.getUsers() != null && !groupUsers.getUsers().isEmpty()) {
            groupUsers.getUsers().
                    forEach(user -> command.getUsers().add(converter.convert(user)));

        }*/

        return command;
    }

}

