package com.simonkulinski.interviewtask.conventers;

import com.simonkulinski.interviewtask.commands.UserCommand;
import com.simonkulinski.interviewtask.model.User;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class UserCommandToUser implements Converter<UserCommand, User> {

    @Synchronized
    @Nullable
    @Override
    public User convert(UserCommand command) {
        final User user = new User();
        user.setId(command.getId());
        user.setFirstName(command.getFirstName());
        user.setLastName(command.getLastName());
        user.setDateOfBirth(command.getDateOfBirth());
        user.setNick(command.getNick());
        user.setPassword(command.getPassword());

        //user.getGroups().contains()
        return user;
    }
}
