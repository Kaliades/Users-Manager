package com.simonkulinski.interviewtask.conventers;

import com.simonkulinski.interviewtask.commands.UserCommand;
import com.simonkulinski.interviewtask.model.User;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class UserToUserCommand implements Converter<User, UserCommand> {

  /*  private final GroupUsersToGroupUsersCommand converter;

    public UserToUserCommand(GroupUsersToGroupUsersCommand converter) {
        this.converter = converter;
    }
*/
    @Synchronized
    @Nullable
    @Override
    public UserCommand convert(User user) {
        final UserCommand command = new UserCommand();
        command.setId(user.getId());
        command.setFirstName(user.getFirstName());
        command.setLastName(user.getLastName());
        command.setDateOfBirth(user.getDateOfBirth());
        command.setNick(user.getNick());
        command.setPassword(user.getPassword());

      /*  if (user.getGroups() != null && !user.getGroups().isEmpty()) {
            user.getGroups().
                    forEach(groupUsers -> command.getGroups().add(converter.convert(groupUsers)));
        }*/

        return command;
    }

}

