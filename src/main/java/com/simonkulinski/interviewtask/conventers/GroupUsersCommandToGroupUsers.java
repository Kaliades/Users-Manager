package com.simonkulinski.interviewtask.conventers;

import com.simonkulinski.interviewtask.commands.GroupUsersCommand;
import com.simonkulinski.interviewtask.model.GroupUsers;
import org.springframework.core.convert.converter.Converter;
import lombok.Synchronized;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class GroupUsersCommandToGroupUsers implements Converter<GroupUsersCommand, GroupUsers> {

   /* private UserCommandToUser converter;

    public GroupUsersCommandToGroupUsers(UserCommandToUser converter) {
        this.converter = converter;
    }*/

    @Synchronized
    @Nullable
    @Override
    public GroupUsers convert(GroupUsersCommand command) {
        final GroupUsers groupUsers = new GroupUsers();
        groupUsers.setId(command.getId());
        groupUsers.setName(command.getName());

     /*   if (command.getUsers() != null && command.getUsers().size() > 0) {
            command.getUsers().
                    forEach((user) -> groupUsers.getUsers().add(converter.convert(user)));
        }*/

        return groupUsers;
    }
}
