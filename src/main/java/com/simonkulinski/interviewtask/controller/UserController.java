package com.simonkulinski.interviewtask.controller;

import com.simonkulinski.interviewtask.commands.UserCommand;
import com.simonkulinski.interviewtask.services.GroupUsersService;
import com.simonkulinski.interviewtask.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {

    private UserService userService;
    private GroupUsersService groupUsersService;

    public UserController(UserService userService, GroupUsersService groupUsersService) {
        this.userService = userService;
        this.groupUsersService = groupUsersService;
    }

    @RequestMapping({"", "/", "/index"})
    public String getAllUsers(Model model) {
        model.addAttribute("users", userService.getAllUsers());
        System.out.println(UserController.class.getSimpleName() + " getAllUsers");
        return "index";
    }

    @RequestMapping("/users/show/{id}")
    public String getUserById(Model model, @PathVariable String id) {
        model.addAttribute("user", userService.getUserById(new Long(id)));
        return "users/show";
    }

    @RequestMapping("/users/new")
    public String newUser(Model model) {
        model.addAttribute("user", new UserCommand());
        model.addAttribute("groups", groupUsersService.getAllGroup());
        return "users/userform";
    }

    @PostMapping("user")
    public String saveOrUpdate(@ModelAttribute UserCommand userCommand) {
        UserCommand user = userService.saveUser(userCommand);
        return "redirect:/users/show/" + user.getId();
    }

    @RequestMapping("/users/delete/{id}")
    public String deleteGroupUsersById(@PathVariable String id) {
        userService.deleteUserById(new Long(id));
        return "redirect:/";
    }

    @RequestMapping("users/update/{id}")
    public String updateUsers(@PathVariable String id, Model model) {
        model.addAttribute("user", userService.findUserCommandById(new Long(id)));
        model.addAttribute("groups", groupUsersService.getAllGroup());
        model.addAttribute("entity", userService.getUserById(new Long(id)));
        return "/users/userform";
    }
}
