package com.simonkulinski.interviewtask.controller;

import com.simonkulinski.interviewtask.commands.GroupUsersCommand;
import com.simonkulinski.interviewtask.model.GroupUsers;
import com.simonkulinski.interviewtask.services.GroupUsersService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GroupUsersController {

    private GroupUsersService service;

    public GroupUsersController(GroupUsersService service) {
        this.service = service;
    }

    @RequestMapping("/groups/groups")
    public String getAllGroups(Model model) {
        model.addAttribute("groups", service.getAllGroup());
        return "groups/groups";
    }

    @RequestMapping("/groups/show/{id}")
    public String getGroupById(Model model, @PathVariable String id) {
        model.addAttribute("group", service.getGroupById(new Long(id)));
        return "groups/show";
    }

    @RequestMapping("/groups/new")
    public String newGroupUsers(Model model) {
        model.addAttribute("group", new GroupUsersCommand());
        return "groups/groupform";
    }

    @PostMapping("group")
    public String saveOrUpdate(@ModelAttribute GroupUsersCommand command) {
        GroupUsersCommand group = service.saveGroupUsers(command);
        return "redirect:/groups/show/" + group.getId();
    }

    @RequestMapping("/groups/delete/{id}")
    public String deleteGroupUsersById(@PathVariable String id) {
        service.deleteGroupUsersById(new Long(id));
        return "redirect:/groups/groups";
    }

    @RequestMapping("groups/update/{id}")
    public String updateGroupUsers(@PathVariable String id, Model model) {
        model.addAttribute("group", service.findGroupUsersCommandById(new Long(id)));
        return "/groups/groupform";
    }
}
