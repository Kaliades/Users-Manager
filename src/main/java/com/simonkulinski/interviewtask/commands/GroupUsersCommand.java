package com.simonkulinski.interviewtask.commands;

public class GroupUsersCommand {

    private Long id;
    private String name;


    public GroupUsersCommand() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "GroupUsersCommand{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
