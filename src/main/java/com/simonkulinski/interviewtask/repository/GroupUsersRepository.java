package com.simonkulinski.interviewtask.repository;

import com.simonkulinski.interviewtask.model.GroupUsers;
import org.springframework.data.repository.CrudRepository;

public interface GroupUsersRepository extends CrudRepository<GroupUsers, Long> {
}
