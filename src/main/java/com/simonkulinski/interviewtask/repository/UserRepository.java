package com.simonkulinski.interviewtask.repository;

import com.simonkulinski.interviewtask.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
